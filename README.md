# Javascript 函数调用 Android 原生 API 说明 #


##**--原生 UI**##

###loadUrl(url)###
说明: 打开新页面

####参数
* url: 新页面 url


----------


###openWebView(url)###
说明: 新建 WebView 并加载页面

####参数
* url: 新页面 url


----------


###openActivity(activityName)###
说明: 打开新 Activity

####参数
* activityName: 所要开启 Activity 类名


----------


###pickDate(successCallback)

说明: 弹出日期选择组件

####参数
* successCallback: (pickDateSuccessCallback)用户选择日期后，执行该回调函数

>####pickDataSuccessCallback(date)
>选择日期成功回调函数

>参数
>
>* date: 所选日期

###示例

    pickDate(onSuccess);
    
    function onSuccess(date){
        console.log(date);
    }


----------


##**--设备部件**##

###openCamera()###

说明: 转到原生照相机应用


----------


##**--存储**##

###var foo = getPreference(key)###

说明: 调用原生 SharedPreferences

####参数
* key: 存储的键(key)

####返回
* value: 键(key)对应应用存储的值，如果没有保存则返回null。

#==数据库调用未完成==#
###getDatabase(successCallback)###

调用原生数据库

####参数
* successCallback: 

原生调用该函数，在前端页面中显示指定的数据库数据


----------


##**--网络通信**##

###connectSocket(dstName, dstPort, resultCallback)###
说明: 连接 socket 到指定地址和端口

####参数
* dstName: 所要连接的主机名或 IP 地址
* dstPort: 所要连接端口
* resultCallback: (connectResultCallback)连接成功回调函数

>###connectResultCallback(result)
>连接结果回调函数

>####参数
>* result: 结果，true：连接成功，false：连接失败

###示例

    connectSocket(connectResult);
    
    function connectResult(result){
        if(result){
            console.log("连接成功");
        }else{
            console.log("连接失败");
        }
    }

----------


###disconnectSocket(resultCallback)###
说明: 断开 socket 连接

####参数
* resultCallback: (disconnectResultCallback)断开成功回调函数

>###disconnectResultCallback(result)
>连接结果回调函数

>####参数
>* result: 结果，true：断开成功，false：断开失败

    disconnectSocket(disconnectResult);
    
    function disconnectResult(result){
        if(result){
            console.log("断开成功");
        }else{
            console.log("断开失败");
        }
    }
    
----------


###isSocketConnecting(stateCallback)###
说明: 判断 socket 是否处在连接状态

####参数
* stateCallback: (connectingStateCallback)连接状态回调函数

>###connectingStateCallback(result)
>连接结果回调函数

>####参数
>* result: 结果，true：连接中，false：未连接

    isSocketConnecting(connectingState);
    
    function connectingState(result){
        if(result){
            console.log("连接中");
        }else{
            console.log("未连接");
        }
    }

----------


###reconnectSocket(resultCallback)###
说明: 重新建立 socket 连接

####参数
* resultCallback: (reconnectResultCallback)重新连接成功回调函数

>###reconnectResultCallback(result)
>连接结果回调函数

>####参数
>* result: 结果，true：重新连接成功，false：重新连接失败

    reconnectSocket(connectingState);
    
    function reconnectResult(result){
        if(result){
            console.log("重新连接成功");
        }else{
            console.log("重新连接失败");
        }
    }