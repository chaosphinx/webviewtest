package test.com.webviewtest.Utils;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Random;

import test.com.webviewtest.model.Contact;


public class Utils {

    public static void initUserHeader(Contact contact) {
        String headerName = " ";
        if (!TextUtils.isEmpty(contact.getName())) {
            headerName = contact.getName();
        }
//        else {
//            headerName = contact.getUserName();
//        }

        if (Character.isDigit(headerName.charAt(0))) {
            contact.setHeader("#");
        } else {
            contact.setHeader(HanziToPinyin.getInstance().get(headerName.substring(0, 1))
                    .get(0).target.substring(0, 1).toUpperCase());
            char header = contact.getHeader().toLowerCase().charAt(0);
            if (header < 'a' || header > 'z') {
                contact.setHeader("#");
            }
        }
    }

    @SuppressLint("DefaultLocale")
    public static class PinyinComparator implements Comparator<Contact> {

        @SuppressLint("DefaultLocale")
        @Override
        public int compare(Contact o1, Contact o2) {
            String py1 = o1.getHeader();
            String py2 = o2.getHeader();

            if (isEmpty(py1) && isEmpty(py2))
                return 0;
            if (isEmpty(py1))
                return -1;
            if (isEmpty(py2))
                return 1;
            String str1 = "";
            String str2 = "";
            try {
                str1 = ((o1.getHeader()).toUpperCase()).substring(0, 1);
                str2 = ((o2.getHeader()).toUpperCase()).substring(0, 1);
            } catch (Exception e) {
                System.out.println("some str is\" \" empty");
            }
            return str1.compareTo(str2);
        }

        private boolean isEmpty(String str) {
            return "".equals(str.trim());
        }
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public static String getDate() {
        Calendar c = Calendar.getInstance();
        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = String.valueOf(c.get(Calendar.MONTH));
        String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH) + 1);
        String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String mins = String.valueOf(c.get(Calendar.MINUTE));
        StringBuffer sbBuffer = new StringBuffer();
        sbBuffer.append(year + "-" + month + "-" + day + " " + hour + ":" + mins);
        return sbBuffer.toString();
    }
}
