package test.com.webviewtest.ui.widget;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

import test.com.webviewtest.R;

public class DatePickerFragment extends DialogFragment {

    Calendar c = Calendar.getInstance();
    int startYear = c.get(Calendar.YEAR);
    int startMonth = c.get(Calendar.MONTH);
    int startDay = c.get(Calendar.DAY_OF_MONTH);
    DatePickerDialog.OnDateSetListener mListener;
//    DatePickerDialog.OnCancelListener mCancelListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                mListener, startYear, startMonth, startDay);
//        dialog.setOnCancelListener(mCancelListener);
//        dialog.getDatePicker().setMinDate();
        return dialog;
    }

    public void setOnDateSetListener(DatePickerDialog.OnDateSetListener listener) {
        mListener = listener;
    }

}
