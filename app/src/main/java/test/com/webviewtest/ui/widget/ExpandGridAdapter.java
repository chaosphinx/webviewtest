package test.com.webviewtest.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import test.com.webviewtest.R;
import test.com.webviewtest.model.User;


public class ExpandGridAdapter extends BaseAdapter {
    public static final int MODE_EDIT_GROUP = 0;
    public static final int MODE_CREATE_GROUP = 0;

    public boolean isDeleteMode;
    public boolean isAdmin;
    private boolean isMoreInfo = true;
    private List<User> userList;
    Context context;

    public ExpandGridAdapter(Context context, List<User> userList) {
        this.userList = userList;
        this.context = context;
        isDeleteMode = false;
    }

    @Override
    public View getView(final int position, View convertView,
                        final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.grid_item_group_members, null);
        }
        ImageView ivAvatar = (ImageView) convertView
                .findViewById(R.id.iv_avatar);
        TextView tvUsername = (TextView) convertView
                .findViewById(R.id.tv_username);
        ImageView ivDeleteBadge = (ImageView) convertView
                .findViewById(R.id.badge_delete);

        // last item is delete btn
        if (position == getCount() - 1 && isAdmin) {
            tvUsername.setText("");
            ivDeleteBadge.setVisibility(View.GONE);
            ivAvatar.setImageResource(R.drawable.icon_btn_deleteperson);

            if (isDeleteMode) {
                // hide delete btn when in delete mode
                convertView.setVisibility(View.GONE);
            } else {
                convertView.setVisibility(View.VISIBLE);
            }

            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isDeleteMode = true;
                    notifyDataSetChanged();
                }
            });

        } else if ((isAdmin && position == getCount() - 2 && isMoreInfo)
                || (!isAdmin && position == getCount() - 1 && isMoreInfo)) {
            // add member btn
            tvUsername.setText("");
            ivDeleteBadge.setVisibility(View.GONE);
            ivAvatar.setImageResource(R.drawable.icon_btn_add_person);
            if (isDeleteMode) {
                // hide add btn when in delete mode
                convertView.setVisibility(View.GONE);
            } else {
                convertView.setVisibility(View.VISIBLE);
            }
            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // start select activity
//                        startActivity((new Intent(
//                                ChatRoomSettingActivity.this,
//                                CreatChatRoomActivity.class).putExtra(
//                                "groupId", groupId)));

                }
            });
        } else { // member item
            User user = userList.get(position);
            String userName = user.getName();
            final String userhid = user.getUserName();
            final String useravatar = user.getAvatar();
            tvUsername.setText(userName);
            ivAvatar.setImageResource(R.drawable.default_useravatar);
            ivAvatar.setTag(useravatar);
            if (useravatar != null && !useravatar.equals("")) {
                Bitmap bitmap;
                int resID = context.getResources().getIdentifier(user.getAvatar(),
                        "drawable", context.getPackageName());
                bitmap = BitmapFactory.decodeResource(context.getResources(), resID);
                if (bitmap != null) {
                    ivAvatar.setImageBitmap(bitmap);
                }
            }

            // demo default avatar
            if (isDeleteMode) {
                // show delete btn when in delete mode
                convertView.findViewById(R.id.badge_delete).setVisibility(
                        View.VISIBLE);
            } else {
                convertView.findViewById(R.id.badge_delete).setVisibility(
                        View.INVISIBLE);
            }
            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDeleteMode) {
                        // if delete user himself, returm
//                            if (EMChatManager.getInstance().getCurrentUser()
//                                    .equals(userhid)) {
//                                startActivity(new Intent(
//                                        ChatRoomSettingActivity.this,
//                                        FXAlertDialog.class).putExtra("msg",
//                                        "cannot delete yourself"));
//                                return;
//                            }
//                            if (!NetUtils.hasNetwork(getApplicationContext())) {
//                                Toast.makeText(
//                                        getApplicationContext(),
//                                        getString(R.string.network_unavailable),
//                                        Toast.LENGTH_SHORT).show();
//                                return;
//                            }
//
//                            deleteMembersFromGroup(userhid);
                        deleteMembersFromGroup(position);
                    } else {
                        // in normal mode, click user start user info activity
                        // startActivity(new
                        // Intent(GroupDetailsActivity.this,
                        // ChatActivity.class).putExtra("userId",
                        // user.getUsername()));
                    }
                }

            });
        }
        return convertView;
    }

    @Override
    public int getCount() {
        if (isAdmin && isMoreInfo) {
            return userList.size() + 2;
        } else if (isMoreInfo) {
            return userList.size() + 1;
        } else {
            return userList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    void deleteMembersFromGroup(int position) {
        userList.remove(position);
        notifyDataSetChanged();
    }

    public void setMoreInfo(boolean isMoreInfo) {
        this.isMoreInfo = isMoreInfo;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
}
