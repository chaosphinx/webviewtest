package test.com.webviewtest.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import test.com.webviewtest.R;


public class AddContactActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout layoutCreateGroup, layoutInvite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        //Setup Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getResources().getString(R.string.title_activity_add_contact));
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        layoutCreateGroup = (LinearLayout) findViewById(R.id.layout_create_group);
        layoutInvite = (LinearLayout) findViewById(R.id.layout_invite_by_phone_number);
        layoutCreateGroup.setOnClickListener(this);
        layoutInvite.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.layout_create_group) {
            Intent intent = new Intent(this, CreateGroupActivity.class);
            startActivity(intent);
            finish();
        } else if (v.getId() == R.id.layout_invite_by_phone_number) {
            Toast.makeText(this, "Invite by phone number", Toast.LENGTH_LONG);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
