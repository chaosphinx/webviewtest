package test.com.webviewtest.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import test.com.webviewtest.Constant;
import test.com.webviewtest.R;
import test.com.webviewtest.Utils.Utils;
import test.com.webviewtest.db.FakeData;
import test.com.webviewtest.model.Contact;
import test.com.webviewtest.model.Conversation;
import test.com.webviewtest.model.Message;
import test.com.webviewtest.model.User;
import test.com.webviewtest.ui.adapter.MessageAdapter;


public class ChatActivity extends AppCompatActivity implements OnClickListener {
    public static final int CHAT_TYPE_PERSON = 0;
    public static final int CHAT_TYPE_GROUP = 1;

    private ImageButton mBtnSend;
    private EditText mEditTextContent;
    private ListView mListView;
    private MessageAdapter mAdapter;
    int chatType;
    int contactId;
    String name, avatar;
    Contact contact;
    int conversation;
    FakeData mFakeData = FakeData.getInstance();
    Conversation c;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        // no keyboard when start activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            contactId = bundle.getInt(Constant.KEY_CONTACT_ID, 0);
            contact = mFakeData.getContactById(contactId);
            name = contact.getName();
            //TODO -1 means no message
            conversation = bundle.getInt("conversation", -1);
            //chatType = bundle.getInt(Constant.KEY_CHAT_TYPE, CHAT_TYPE_PERSON);
            if (contact.isGroup()) {
                chatType = CHAT_TYPE_GROUP;
            } else {
                chatType = CHAT_TYPE_PERSON;
                avatar = bundle.getString("avatar", "R.drawable.default_useravatar");
            }
        }

        initView();

//        initData();
        if (conversation != -1) {
            c = mFakeData.getConversationList().get(conversation);
        } else {
            c = new Conversation(contact);
        }
        mAdapter = new MessageAdapter(this, c);
        mListView.setAdapter(mAdapter);
    }


    public void initView() {
        //Setup Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(name);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        mListView = (ListView) findViewById(R.id.listview);
        mListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        mListView.setStackFromBottom(true);
        mBtnSend = (ImageButton) findViewById(R.id.btn_send);
        mBtnSend.setOnClickListener(this);

        mEditTextContent = (EditText) findViewById(R.id.et_sendmessage);
    }

//    private String[] msgArray = new String[]{"1212", "2212", "2323", "1223wf", "1212", "2212", "2323", "1223wf",};
//
//    private String[] dataArray = new String[]{"2012-09-01 18:00", "2012-09-01 18:10",
//            "2012-09-01 18:11", "2012-09-01 18:20",
//            "2012-09-01 18:30", "2012-09-01 18:35",
//            "2012-09-01 18:40", "2012-09-01 18:50"};
//    private final static int COUNT = 8;
//
//    public void initData() {
//        for (int i = 0; i < COUNT; i++) {
//            Message entity = new Message();
//            entity.setDate(dataArray[i]);
//            if (i % 2 == 0) {
//                entity.setName("qwqw");
//                entity.setMsgType(true);
//            } else {
//                entity.setName("jkljk");
//                entity.setMsgType(false);
//            }
//
//            entity.setText(msgArray[i]);
//            mDataArrays.add(entity);
//        }
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_info) {
            if (chatType == CHAT_TYPE_PERSON) {
                Intent intent = new Intent(this, InfoUserActivity.class);
                intent.putExtra(Constant.KEY_CONTACT_ID, contactId);
                if (conversation != -1) {
                    User user = (User) FakeData.getInstance().getConversationList().get(conversation).getSender();
                    intent.putExtra("userName", name);
                    intent.putExtra("avatar", avatar);
                    intent.putExtra("sign", user.getSign());
                    intent.putExtra("userId", user.getUserId());
                }
                startActivity(intent);
            } else if (chatType == CHAT_TYPE_GROUP) {
                Intent intent = new Intent(this, InfoGroupActivity.class);
                intent.putExtra(Constant.KEY_CONTACT_ID, contactId);
                startActivity(intent);
            }
            return true;
        } else if (id == android.R.id.home) {
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
                send();
                break;
        }
    }

    private void send() {
        String contString = mEditTextContent.getText().toString();
        if (contString.length() > 0) {
            Message newMessage = new Message();
            newMessage.setDate(Utils.getDate());
            newMessage.setName(" ");
            newMessage.setMsgType(false);
            newMessage.setText(contString);

            //mDataArrays.add(newMessage);
            if (c.getMsgCount() == 0) {
                mFakeData.addConversation(c);
            }
            c.addMessage(newMessage);
            mAdapter.notifyDataSetChanged();

            mEditTextContent.setText("");

            mListView.setSelection(mListView.getCount() - 1);
        }
    }
}
