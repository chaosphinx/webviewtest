package test.com.webviewtest.ui;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import test.com.webviewtest.R;

public class HybridActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hybrid);
        HybridFragment hybridFragment = new HybridFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, hybridFragment,
                "hybridFragment").commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.container);
            if (currentFrag instanceof HybridFragment) {
                ((HybridFragment) currentFrag).getWebView().reload();
            } else if (currentFrag instanceof WebViewFragment) {
                ((WebViewFragment) currentFrag).getWebView().reload();
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
