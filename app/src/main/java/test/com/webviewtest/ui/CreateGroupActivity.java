package test.com.webviewtest.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import test.com.webviewtest.Constant;
import test.com.webviewtest.R;
import test.com.webviewtest.Utils.Utils;
import test.com.webviewtest.db.FakeData;
import test.com.webviewtest.model.Contact;
import test.com.webviewtest.model.Group;
import test.com.webviewtest.model.User;
import test.com.webviewtest.ui.widget.ExpandGridAdapter;
import test.com.webviewtest.ui.widget.ExpandGridView;


public class CreateGroupActivity extends AppCompatActivity {

    private ListView listView;
    List<User> contactList;

    private PickContactAdapter contactAdapter;
    /**
     * members already exist in group
     */
    private List<String> exitingMembers = new ArrayList<String>();

    ScrollView mScrollView;
    ExpandGridAdapter mGridSelectedUserAdapter;
    ExpandGridView mGridViewSelectedUser;
    TextView tvSelectHint;

    // total user number
    int total = 0;
    private String userId = null;
    private String groupId = null;
    private ProgressDialog progressDialog;
    private String groupname;

    private List<String> addList = new ArrayList<>();
    private List<User> selectedUserList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        //Setup Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getResources().getString(R.string.title_activity_create_group));
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        progressDialog = new ProgressDialog(this);
        userId = getIntent().getStringExtra("userId");

//        tv_checked = (TextView) this.findViewById(R.id.tv_checked);

        if (userId != null) {
            exitingMembers.add(userId);
            total = 1;
            addList.add(userId);
        }

        // fetch contact list
//        final List<User> alluserList = new ArrayList<User>();
//        for (User user : DemoApplication.getInstance().getContactList()
//                .values()) {
//            if (!user.getUsername().equals(Constant.NEW_FRIENDS_USERNAME)
//                    & !user.getUsername().equals(Constant.GROUP_USERNAME))
//        alluserList.add(user);
//        }
        initFakeData();

        // sort list by alphabet
        Collections.sort(contactList, new Utils.PinyinComparator() {
        });

        listView = (ListView) findViewById(R.id.list_create_group_contact);

        mScrollView = (ScrollView) findViewById(R.id.scroll_view_selected_member);
        mGridViewSelectedUser = (ExpandGridView) findViewById(R.id.gridview_selected_user);
        mGridSelectedUserAdapter = new ExpandGridAdapter(this, selectedUserList);
        mGridSelectedUserAdapter.setMoreInfo(false);
        mGridViewSelectedUser.setAdapter(mGridSelectedUserAdapter);

        tvSelectHint = (TextView) findViewById(R.id.tv_select_hint);
        contactAdapter = new PickContactAdapter(this, contactList);
        listView.setAdapter(contactAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                checkBox.toggle();
            }
        });

//        tv_checked.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                save();
//            }
//        });
    }

    void initFakeData() {
        contactList = new ArrayList<>();
        for (Contact contact : FakeData.getInstance().getContactList()) {
            if (contact.getClass() == User.class) {
                contactList.add((User) contact);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        if (item.getItemId() == R.id.action_confirm) {
            //TODO add new group
            Group group = new Group();
            if (selectedUserList.size() > 0) {
                for (User user : selectedUserList) {
                    group.addUser(user);
                }
                FakeData.getInstance().addGroup(group);
                Intent intent = new Intent(this, ChatActivity.class);
                intent.putExtra(Constant.KEY_CONTACT_ID, group.getContactId());
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, "need to select at least one user", Toast.LENGTH_LONG).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    // show avatar in top gridview
    private void addAvatar(Bitmap bitmap, User glufineid) {
        if (exitingMembers.contains(glufineid.getUserName()) && groupId != null) {
            return;
        }
        if (addList.contains(glufineid.getUserName())) {
            return;
        }
        total++;
        tvSelectHint.setVisibility(View.GONE);

        // paramater settings
//        android.widget.LinearLayout.LayoutParams menuLinerLayoutParames = new LinearLayout.LayoutParams(
//                108, 108, 1);
//        View view = LayoutInflater.from(this).inflate(
//                R.layout.list_item_create_group_selected, null);
//        ImageView images = (ImageView) view.findViewById(R.id.iv_avatar);
//        menuLinerLayoutParames.setMargins(6, 6, 6, 6);

        // set tag for deleting
//        view.setTag(glufineid);
//        if (bitmap == null) {
//            images.setImageResource(R.drawable.default_useravatar);
//        } else {
//            images.setImageBitmap(bitmap);
//        }

        selectedUserList.add(glufineid);
        mGridSelectedUserAdapter.notifyDataSetChanged();
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });

//        mLayoutSelectedUser.addView(view, menuLinerLayoutParames);
        addList.add(glufineid.getUserName());
    }

    private void deleteAvatar(User glufineid) {
//        View view = mLayoutSelectedUser.findViewWithTag(glufineid);
//
//        mLayoutSelectedUser.removeView(view);
        total--;
        if (total == 0) {
            tvSelectHint.setVisibility(View.VISIBLE);
        }
        selectedUserList.remove(glufineid);
        mGridSelectedUserAdapter.notifyDataSetChanged();

        addList.remove(glufineid.getUserName());
    }

    private class PickContactAdapter extends BaseAdapter {

        private Context mContext;
        private boolean[] isCheckedArray;
        private Bitmap[] bitmaps;
        private List<User> list = new ArrayList<>();

        public PickContactAdapter(Context context, List<User> users) {
            mContext = context;
            this.list = users;
            bitmaps = new Bitmap[list.size()];
            isCheckedArray = new boolean[list.size()];
        }

        public Bitmap getBitmap(int position) {
            return bitmaps[position];
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_create_group_contact, null);
            ImageView ivAvatar = (ImageView) convertView
                    .findViewById(R.id.iv_avatar);
            TextView tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            TextView tvAlphabeticalDivider = (TextView) convertView
                    .findViewById(R.id.alphabetical_divider);
            final User user = list.get(position);

            final String avatar = user.getAvatar();
            String name = user.getName();
            String header = user.getHeader();
            final String username = user.getUserName();
            tv_name.setText(name);
            int resID = getResources().getIdentifier(avatar, "drawable", getPackageName());
            ivAvatar.setImageResource(R.drawable.default_useravatar);
            ivAvatar.setTag(avatar);
            Bitmap bitmap;
            if (avatar != null && !avatar.equals("")) {
                bitmap = BitmapFactory.decodeResource(getResources(), resID);
                if (bitmap != null) {
                    ivAvatar.setImageBitmap(bitmap);
                }
                bitmaps[position] = bitmap;
            }
            if (position == 0 || header != null
                    && !header.equals(getItem(position - 1))) {
                if ("".equals(header)) {
                    tvAlphabeticalDivider.setVisibility(View.GONE);
                } else {
                    tvAlphabeticalDivider.setVisibility(View.VISIBLE);
                    tvAlphabeticalDivider.setText(header.toUpperCase());
                }
            } else {
                tvAlphabeticalDivider.setVisibility(View.GONE);
            }

            // select checkbox
            final CheckBox checkBox = (CheckBox) convertView
                    .findViewById(R.id.checkbox);

            if (exitingMembers != null && exitingMembers.contains(username)) {
                checkBox.setButtonDrawable(R.drawable.btn_check);
            } else {
                checkBox.setButtonDrawable(R.drawable.check_blue);
            }

            if (addList != null && addList.contains(username)) {
                checkBox.setChecked(true);
                isCheckedArray[position] = true;
            }
            if (checkBox != null) {
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // checkbox of existed users are selected
                        if (exitingMembers.contains(username)) {
                            isChecked = true;
                            checkBox.setChecked(true);
                        }
                        isCheckedArray[position] = isChecked;

                        if (isChecked) {
                            addAvatar(contactAdapter.getBitmap(position),
                                    list.get(position));

                        } else {
                            deleteAvatar(list.get(position));
                        }
                    }
                });
                // checkbox of existed users are selected
                if (exitingMembers.contains(username)) {
                    checkBox.setChecked(true);
                    isCheckedArray[position] = true;
                } else {
                    checkBox.setChecked(isCheckedArray[position]);
                }
            }
            return convertView;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public String getItem(int position) {
            if (position < 0) {
                return "";
            }
            return list.get(position).getHeader();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }
}
