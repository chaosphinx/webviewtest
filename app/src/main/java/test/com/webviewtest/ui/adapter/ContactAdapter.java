package test.com.webviewtest.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.ArrayList;
import java.util.List;

import test.com.webviewtest.R;
import test.com.webviewtest.Utils.GroupAvatar.GroupFaceUtil;
import test.com.webviewtest.db.FakeData;
import test.com.webviewtest.model.Contact;
import test.com.webviewtest.model.Group;

public class ContactAdapter extends RecyclerSwipeAdapter<ContactAdapter.ContactViewHolder> {

    Context mContext;
    List<String> list;
    onItemClickListener mListener;
    FakeData mFakeData;

    public ContactAdapter(Context context, onItemClickListener listener) {
        mContext = context;
        list = new ArrayList<>();
        mListener = listener;
        mFakeData = FakeData.getInstance();
    }

    @Override
    public ContactAdapter.ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_contact, parent, false);
        ContactAdapter.ContactViewHolder vh = new ContactAdapter.ContactViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ContactAdapter.ContactViewHolder holder, final int position) {
        Contact contact = mFakeData.getContactList().get(position);
        holder.tvName.setText(contact.getName());
        //set avatar
        if (contact.isGroup()) {
            ArrayList<Bitmap> bmpList = new ArrayList<>();
            for (int i = 0; i < ((Group) contact).getUserNumber() - 1 && i < 9; i++) {
                int resID = mContext.getResources().getIdentifier(((Group) contact).getUser(i).getAvatar(),
                        "drawable", mContext.getPackageName());
                Bitmap avatar = BitmapFactory.decodeResource(mContext.getResources(), resID);
                bmpList.add(avatar);
            }
            holder.ivAvatar.setImageBitmap(GroupFaceUtil.createGroupFace(GroupFaceUtil.FACETYPE_WS,
                    mContext, bmpList.toArray(new Bitmap[bmpList.size()])));
        } else { // person
            int resID = mContext.getResources().getIdentifier(contact.getAvatar(), "drawable", mContext.getPackageName());
            holder.ivAvatar.setImageResource(resID);
        }
        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });

        holder.btnTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = list.get(position);
                list.remove(position);
                list.add(0, s);
                notifyDataSetChanged();
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, list.size());
            }
        });

    }


    public void add(String s) {
        list.add(s);
    }

    @Override
    public int getItemCount() {
        return mFakeData.getContactList().size();
    }

    @Override
    public int getSwipeLayoutResourceId(int i) {
        return R.id.swipe;
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout;
        ImageView ivAvatar;
        TextView tvName;
        SwipeLayout swipeLayout;
        TextView btnTop;
        TextView btnDelete;

        public ContactViewHolder(View v) {
            super(v);
            layout = (RelativeLayout) v.findViewById(R.id.layout);
            ivAvatar = (ImageView) v.findViewById(R.id.iv_avatar);
            tvName = (TextView) v.findViewById(R.id.tv_name);
            swipeLayout = (SwipeLayout) v.findViewById(R.id.swipe);
            btnTop = (TextView) v.findViewById(R.id.btn_top);
            btnDelete = (TextView) v.findViewById(R.id.btn_delete);
        }
    }

    public interface onItemClickListener {
        void onItemClick(int i);
    }
}
