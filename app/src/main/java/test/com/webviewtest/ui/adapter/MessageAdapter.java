package test.com.webviewtest.ui.adapter;

import android.content.Context;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import test.com.webviewtest.Constant;
import test.com.webviewtest.R;
import test.com.webviewtest.Utils.Utils;
import test.com.webviewtest.model.Conversation;
import test.com.webviewtest.model.Group;
import test.com.webviewtest.model.Message;
import test.com.webviewtest.model.User;
import test.com.webviewtest.ui.InfoUserActivity;


public class MessageAdapter extends BaseAdapter {

    public static interface IMsgViewType {
        int IMVT_COM_MSG = 0;
        int IMVT_TO_MSG = 1;
    }

    private Conversation mConversation;

    private Context mContext;

    private LayoutInflater mInflater;

    public MessageAdapter(Context context, Conversation conversation) {
        mContext = context;
        mConversation = conversation;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return mConversation.getMsgCount();
    }

    public Object getItem(int position) {
        return mConversation.getMessage(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public int getItemViewType(int position) {
        Message message = mConversation.getMessage(position);

        if (message.getMsgType()) {
            return IMsgViewType.IMVT_COM_MSG;
        } else {
            return IMsgViewType.IMVT_TO_MSG;
        }
    }


    public int getViewTypeCount() {
        return 2;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        if (mConversation != null) {

            Message message = mConversation.getMessage(position);
            boolean isComMsg = message.getMsgType();

            ViewHolder viewHolder;
            if (convertView == null) {
                if (isComMsg) {
                    convertView = mInflater.inflate(R.layout.list_item_msg_left, null);
                } else {
                    convertView = mInflater.inflate(R.layout.list_item_msg_right, null);
                }

                viewHolder = new ViewHolder();
                viewHolder.tvSendTime = (TextView) convertView.findViewById(R.id.tv_sendtime);
                viewHolder.tvUserName = (TextView) convertView.findViewById(R.id.tv_username);
                viewHolder.tvContent = (TextView) convertView.findViewById(R.id.tv_chatcontent);
                if (isComMsg) {
                    viewHolder.ivAvatar = (ImageView) convertView.findViewById(R.id.iv_avatar);
                }
                viewHolder.isComMsg = isComMsg;

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.tvSendTime.setText(message.getDate());
            viewHolder.tvUserName.setText(message.getName());
            viewHolder.tvContent.setText(message.getText());
            if (isComMsg && mConversation != null) {
                int resID = R.drawable.default_useravatar;
                if (mConversation.isGroup()) {
                    Group group = (Group) mConversation.getSender();
                    final User randomUser = group.getUser(Utils.randInt(0, group.getUserNumber() - 2));
                    viewHolder.tvUserName.setText(randomUser.getName());
                    resID = mContext.getResources().getIdentifier(randomUser.getAvatar(),
                            "drawable", mContext.getPackageName());
                    viewHolder.ivAvatar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, InfoUserActivity.class);
                            intent.putExtra(Constant.KEY_CONTACT_ID, randomUser.getContactId());
                            mContext.startActivity(intent);
                        }
                    });
                } else {
                    resID = mContext.getResources().getIdentifier(mConversation.getSender().getAvatar(),
                            "drawable", mContext.getPackageName());
                    viewHolder.ivAvatar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, InfoUserActivity.class);
                            intent.putExtra(Constant.KEY_CONTACT_ID, mConversation.getSender().getContactId());
                            mContext.startActivity(intent);
                        }
                    });
                }
                viewHolder.ivAvatar.setImageResource(resID);
            }

        }
        return convertView;
    }


    static class ViewHolder {
        public TextView tvSendTime;
        public TextView tvUserName;
        public TextView tvContent;
        public ImageView ivAvatar;
        public boolean isComMsg = true;
    }


}