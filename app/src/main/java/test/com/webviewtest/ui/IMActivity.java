package test.com.webviewtest.ui;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import test.com.webviewtest.R;
import test.com.webviewtest.db.FakeData;

public class IMActivity extends AppCompatActivity implements View.OnClickListener {
    final static int FRAG_MESSAGE = 0;
    final static int FRAG_CONTACT = 1;

    TextView tvMessage, tvContact;
    ConversationFragment mMessageFragment;
    ContactFragment mContactFragment;
    FragmentManager mFragmentManager;
    int currentTab;

    //UserDao userDao;
    FakeData mFakeData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_im);
        initView();
        mFragmentManager = getSupportFragmentManager();
        currentTab = FRAG_MESSAGE;
        setTabSelection(currentTab);
//        userDao = new UserDao(this);
        mFakeData = FakeData.getInstance();
    }

    void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
//            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        tvMessage = (TextView) findViewById(R.id.btn_message);
        tvContact = (TextView) findViewById(R.id.btn_contact);
        tvMessage.setOnClickListener(this);
        tvContact.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_im, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            Intent intent = new Intent(this, AddContactActivity.class);
            startActivity(intent);
            return true;
        } else if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_message) {
            setTabSelection(FRAG_MESSAGE);
        } else if (v.getId() == R.id.btn_contact) {
            setTabSelection(FRAG_CONTACT);
        }
    }

    void setTabSelection(int tab) {
        setTabStyle(tab);
        // start transaction
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        hideFragment(transaction);
        switch (tab) {
            case FRAG_MESSAGE:
                if (mMessageFragment == null) {
                    mMessageFragment = new ConversationFragment();
                    transaction.add(R.id.container, mMessageFragment);
                } else {
                    transaction.show(mMessageFragment);
                }
                break;

            case FRAG_CONTACT:
                if (mContactFragment == null) {
                    mContactFragment = new ContactFragment();
                    transaction.add(R.id.container, mContactFragment);
                } else {
                    transaction.show(mContactFragment);
                }
                break;
        }
        transaction.commit();
    }

    void setTabStyle(int frag) {
        if (frag == FRAG_MESSAGE) {
            tvMessage.setBackgroundResource(R.drawable.tab_left_active_bg);
            tvMessage.setTextColor(getResources().getColor(R.color.white));
            tvContact.setBackgroundResource(R.drawable.tab_right_normal_bg);
            tvContact.setTextColor(getResources().getColor(R.color.green_main));
        } else if (frag == FRAG_CONTACT) {
            tvContact.setBackgroundResource(R.drawable.tab_right_active_bg);
            tvContact.setTextColor(getResources().getColor(R.color.white));
            tvMessage.setBackgroundResource(R.drawable.tab_left_normal_bg);
            tvMessage.setTextColor(getResources().getColor(R.color.green_main));
        }
    }

    void hideFragment(FragmentTransaction transaction) {
        if (mContactFragment != null) {
            transaction.hide(mContactFragment);
        }
        if (mMessageFragment != null) {
            transaction.hide(mMessageFragment);
        }
    }
}
