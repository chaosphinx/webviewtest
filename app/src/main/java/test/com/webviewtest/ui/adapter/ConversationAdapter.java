package test.com.webviewtest.ui.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.ArrayList;
import java.util.List;

import test.com.webviewtest.R;
import test.com.webviewtest.Utils.GroupAvatar.GroupFaceUtil;
import test.com.webviewtest.db.FakeData;
import test.com.webviewtest.model.Contact;
import test.com.webviewtest.model.Conversation;
import test.com.webviewtest.model.Group;


public class ConversationAdapter extends RecyclerSwipeAdapter<ConversationAdapter.MessageViewHolder> {

    Context mContext;
    List<String> list;
    onItemClickListener mListener;
    FakeData mFakeData;

    public ConversationAdapter(Context context, onItemClickListener listener) {
        mContext = context;
        list = new ArrayList<>();
        mListener = listener;
        mFakeData = FakeData.getInstance();
    }

    @Override
    public ConversationAdapter.MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_conversation, parent, false);
        ConversationAdapter.MessageViewHolder vh = new ConversationAdapter.MessageViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ConversationAdapter.MessageViewHolder holder, final int position) {
        Contact sender = mFakeData.getConversationList().get(position).getSender();
        Conversation c = mFakeData.getConversationList().get(position);
        holder.tvName.setText(sender.getName());
        //set avatar
        if (sender.isGroup()) {
            ArrayList<Bitmap> bmpList = new ArrayList<>();
            for (int i = 0; i < ((Group) sender).getUserNumber() - 1 && i < 9; i++) {
                int resID = mContext.getResources().getIdentifier(((Group) sender).getUser(i).getAvatar(),
                        "drawable", mContext.getPackageName());
                Bitmap avatar = BitmapFactory.decodeResource(mContext.getResources(), resID);
                bmpList.add(avatar);
            }
            holder.ivAvatar.setImageBitmap(GroupFaceUtil.createGroupFace(GroupFaceUtil.FACETYPE_WS,
                    mContext, bmpList.toArray(new Bitmap[bmpList.size()])));
        } else { // person
            int resID = mContext.getResources().getIdentifier(sender.getAvatar(), "drawable", mContext.getPackageName());
            holder.ivAvatar.setImageResource(resID);
        }
        if (c.getUnreadMsgCount() > 0) {
            if (c.getUnreadMsgCount() <= 99) {
                holder.tvUnread.setText(String.valueOf(c.getUnreadMsgCount()));
            } else {
                holder.tvUnread.setText("...");
            }
        } else {
            holder.tvUnread.setVisibility(View.GONE);
        }
        if (mFakeData.getConversationList().get(position).getLastMessage().getText() != null) {
            holder.tvContent.setText(mFakeData.getConversationList().get(position).getLastMessage().getText());
        } else {
            holder.tvContent.setText("null");
        }
        holder.tvTime.setText("7-22");
        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });

        holder.btnTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Conversation c = mFakeData.getConversationList().get(position);
                mFakeData.getConversationList().remove(c);
                mFakeData.getConversationList().add(0, c);
                notifyDataSetChanged();
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("确认删除该条记录？");
                builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Conversation c = mFakeData.getConversationList().get(position);
                        mFakeData.getConversationList().remove(c);
                        //list.remove(position);
                        notifyItemRemoved(position);
                        notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();

                dialog.show();
            }
        });

    }

    public void add(String s) {
//        list.add(s);
    }

    @Override
    public int getItemCount() {
        return mFakeData.getConversationList().size();
    }

    @Override
    public int getSwipeLayoutResourceId(int i) {
        return R.id.swipe;
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout;
        ImageView ivAvatar;
        TextView tvUnread;
        TextView tvName;
        TextView tvContent;
        TextView tvTime;
        SwipeLayout swipeLayout;
        TextView btnTop;
        TextView btnDelete;

        public MessageViewHolder(View v) {
            super(v);
            layout = (RelativeLayout) v.findViewById(R.id.layout);
            ivAvatar = (ImageView) v.findViewById(R.id.iv_avatar);
            tvUnread = (TextView) v.findViewById(R.id.tv_unread);
            tvName = (TextView) v.findViewById(R.id.tv_name);
            tvContent = (TextView) v.findViewById(R.id.tv_content);
            tvTime = (TextView) v.findViewById(R.id.tv_time);
            swipeLayout = (SwipeLayout) v.findViewById(R.id.swipe);
            btnTop = (TextView) v.findViewById(R.id.btn_top);
            btnDelete = (TextView) v.findViewById(R.id.btn_delete);
        }
    }

    public interface onItemClickListener {
        void onItemClick(int i);
    }
}
