package test.com.webviewtest.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import test.com.webviewtest.Constant;
import test.com.webviewtest.R;
import test.com.webviewtest.db.FakeData;
import test.com.webviewtest.model.User;


public class InfoUserActivity extends AppCompatActivity implements View.OnClickListener {
    int contactId;
    TextView tvNickName, tvRegisterNumber, tvSignature, tvContact;
    ImageView ivAvatar;
    Button btnIM;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            contactId = bundle.getInt(Constant.KEY_CONTACT_ID);
            user = (User) FakeData.getInstance().getContactById(contactId);
            initView();
            initInfo();
        }
    }

    void initView() {
        //Setup Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(user.getName());
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        ivAvatar = (ImageView) findViewById(R.id.iv_avatar);
        tvNickName = (TextView) findViewById(R.id.tv_nick_name);
        tvRegisterNumber = (TextView) findViewById(R.id.tv_register_number);
        tvSignature = (TextView) findViewById(R.id.tv_signature);
        tvContact = (TextView) findViewById(R.id.tv_contact_title);
        btnIM = (Button) findViewById(R.id.btn_im);
        btnIM.setOnClickListener(this);
    }

    void initInfo() {
        int resID = getResources().getIdentifier(user.getAvatar(), "drawable", getPackageName());
        ivAvatar.setImageResource(resID);
        tvNickName.setText(this.getResources().getString(R.string.nick_name_title) + user.getName());
        tvRegisterNumber.setText(this.getResources().getString(R.string.register_number_title) + String.valueOf(contactId));
        tvSignature.setText(this.getResources().getString(R.string.signature_title) + user.getSign());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_im) {
            finish();
        }
    }
}
