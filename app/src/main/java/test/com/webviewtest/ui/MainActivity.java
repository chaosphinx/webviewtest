package test.com.webviewtest.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import test.com.webviewtest.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button mBtnGotoHtml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBtnGotoHtml = (Button) findViewById(R.id.btn_goto_html);
        mBtnGotoHtml.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_goto_html:
                Intent intent = new Intent(this, HybridActivity.class);
                startActivity(intent);
                break;
        }
    }
}
