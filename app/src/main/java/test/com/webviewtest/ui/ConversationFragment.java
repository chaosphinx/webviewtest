package test.com.webviewtest.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import test.com.webviewtest.Constant;
import test.com.webviewtest.R;
import test.com.webviewtest.db.FakeData;
import test.com.webviewtest.model.Conversation;
import test.com.webviewtest.ui.adapter.AdapterDivider;
import test.com.webviewtest.ui.adapter.ConversationAdapter;

public class ConversationFragment extends Fragment implements ConversationAdapter.onItemClickListener {
    private ConversationAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    RecyclerView mRecyclerView;

    public ConversationFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_conversation, null);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.list_message);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ConversationAdapter(getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new AdapterDivider(getActivity(), R.drawable.list_divider));
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int i) {
        Log.i("item click", String.valueOf(i));
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        Conversation c = FakeData.getInstance().getConversationList().get(i);
        // clear unread
        if (c.getUnreadMsgCount() > 0) {
            c.setUnreadMsgCount(0);
        }
        if (c.isGroup()) {
            intent.putExtra(Constant.KEY_CHAT_TYPE, ChatActivity.CHAT_TYPE_GROUP);
        } else {
            intent.putExtra(Constant.KEY_CHAT_TYPE, ChatActivity.CHAT_TYPE_PERSON);
            intent.putExtra("avatar", c.getSender().getAvatar());
        }
        intent.putExtra("name", c.getSender().getName());
        intent.putExtra(Constant.KEY_CONTACT_ID, c.getSender().getContactId());
        intent.putExtra("conversation", i);
        startActivity(intent);
    }
}
