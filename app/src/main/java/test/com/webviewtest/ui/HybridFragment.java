package test.com.webviewtest.ui;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.DatePicker;

import java.io.IOException;
import java.net.Socket;

import test.com.webviewtest.Interface.SocketCompleteListener;
import test.com.webviewtest.R;
import test.com.webviewtest.Task.SocketTask;
import test.com.webviewtest.db.DbHelper;
import test.com.webviewtest.db.UserContract;
import test.com.webviewtest.ui.widget.DatePickerFragment;

public class HybridFragment extends Fragment implements DatePickerDialog.OnDateSetListener, View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;

    private static final String KEY_PREFS = "key_prefs";
    WebView mWebView;
    WebSettings wSettings;
    Button mBtn1, mBtn2, mBtn3;
    SharedPreferences prefs;
    DbHelper mDbHelper;
    Socket mSocket;

    public static HybridFragment newInstance(String param1, String param2) {
        HybridFragment fragment = new HybridFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public HybridFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mDbHelper = new DbHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hybrid, container, false);
        mWebView = (WebView) view.findViewById(R.id.webview);
        mBtn1 = (Button) view.findViewById(R.id.btn_html_1);
        mBtn2 = (Button) view.findViewById(R.id.btn_html_2);
        mBtn3 = (Button) view.findViewById(R.id.btn_html_3);
        mBtn1.setOnClickListener(this);
        mBtn2.setOnClickListener(this);
        mBtn3.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupWebView();
        initSharedPrefs();
        initDatabase();
        mSocket = null;
    }

    void setupWebView() {
        mWebView.setClickable(true);
        wSettings = mWebView.getSettings();
        wSettings.setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.addJavascriptInterface(new JsInterface(getActivity()), "Android");
        mWebView.loadUrl("file:///android_asset/WD-UI-APP/myhtml.html");
    }

    void initSharedPrefs() {
        SharedPreferences.Editor ed = prefs.edit();
        ed.putString("key_prefs", "get prefs success");
        ed.apply();
    }

    void initDatabase() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                SQLiteDatabase db = mDbHelper.getWritableDatabase();
                ContentValues jason = new ContentValues();
                jason.put(UserContract.UserEntry.COLUMN_USER_ID, "23234");
                jason.put(UserContract.UserEntry.COLUMN_NAME, "Jason");
                jason.put(UserContract.UserEntry.COLUMN_AGE, "34");
                long jasonId = db.insert(UserContract.UserEntry.TABLE_NAME, null, jason);

                ContentValues steve = new ContentValues();
                steve.put(UserContract.UserEntry.COLUMN_USER_ID, "9672");
                steve.put(UserContract.UserEntry.COLUMN_NAME, "Steve");
                steve.put(UserContract.UserEntry.COLUMN_AGE, "12");
                long steveId = db.insert(UserContract.UserEntry.TABLE_NAME, null, steve);
                Log.i("database init", String.valueOf(jasonId) + String.valueOf(steveId));
            }
        });
        thread.start();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_html_1:
                mWebView.loadUrl("file:///android_asset/WD-UI-APP/myhtml.html");
                break;
            case R.id.btn_html_2:
                mWebView.loadUrl("file:///android_asset/WD-UI-APP/login.html");
                break;
            case R.id.btn_html_3:
                mWebView.loadUrl("file:///android_asset/WD-UI-APP/temp.html");
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String s = String.valueOf(year) + String.valueOf(monthOfYear + 1) + String.valueOf(dayOfMonth);
        mWebView.loadUrl("javascript:getDate(" + s + ")");
        Log.i("date", s);
    }


    public class JsInterface {
        Context mContext;

        public JsInterface(Context context) {
            mContext = context;
        }

        @JavascriptInterface
        public void openActivity(String activityName) {
            Class<?> activityClass = null;
            try {
                activityClass = Class.forName("test.com.webviewtest.ui." + activityName);
            } catch (ClassNotFoundException e) {
                Log.e("openActivity", e.toString());
            }
            Intent intent = new Intent(mContext, activityClass);
            mContext.startActivity(intent);
        }

        @JavascriptInterface
        public void loadUrl(final String url) {
            mWebView.post(new Runnable() {
                @Override
                public void run() {
                    mWebView.loadUrl("file:///android_asset/WD-UI-APP/" + url);
                }
            });
//            Intent intent = new Intent(mContext, HybridActivity.class);
//            intent.putExtra("url", url);
//            mContext.startActivity(intent);
        }

        @JavascriptInterface
        public void showDatePicker(final String callbackFunction) {
            DatePickerFragment datePicker = new DatePickerFragment();
            DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    String s = String.valueOf(year) + String.valueOf(monthOfYear + 1) + String.valueOf(dayOfMonth);
                    mWebView.loadUrl("javascript:" + callbackFunction + "(" + s + ")");
                }
            };
            long minDate, maxDate;
            datePicker.setOnDateSetListener(listener);
            datePicker.show((getActivity()).getSupportFragmentManager(), "datepicker");
        }

        @JavascriptInterface
        public void showConfirmationDialog(String title, String message, final String callbackFunction) {
            Dialog.OnClickListener positiveListener = new Dialog.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //mWebView.loadUrl("javascript:" + callbackFunction + "('" + data + "', true)");
                    mWebView.post(new Runnable() {
                        @Override
                        public void run() {
                            mWebView.loadUrl("javascript:" + callbackFunction + "(true)");
                        }
                    });
                }
            };
            Dialog.OnClickListener negativeListener = new Dialog.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //mWebView.loadUrl("javascript:" + callbackFunction + "('" + data + "', false)");
                    mWebView.post(new Runnable() {
                        @Override
                        public void run() {
                            mWebView.loadUrl("javascript:" + callbackFunction + "(false)");
                        }
                    });
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder
                    .setTitle(title).setMessage(message)
                    .setPositiveButton("Ok", positiveListener)
                    .setNegativeButton("Cancel", negativeListener);
//                    .setCancelable(false);
            builder.create().show();
        }

        @JavascriptInterface
        public void openCamera() {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivity(cameraIntent);
        }

        @JavascriptInterface
        public void setPrefs(String value) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(KEY_PREFS, value);
            editor.apply();
        }

        @JavascriptInterface
        public String getPreference(final String key) {
            Log.i("getPrefs", key);
            return prefs.getString(key, "null");
//            mWebView.post(new Runnable() {
//                @Override
//                public void run() {
//                    //mWebView.loadUrl("javascript:" + callbackFunction + "('" + s + "')");
//                }
//            });
        }

        @JavascriptInterface
        public void getDatabase(final String callbackFunction) {
            mWebView.post(new Runnable() {
                @Override
                public void run() {
                    SQLiteDatabase db = mDbHelper.getReadableDatabase();
                    String[] projection = {UserContract.UserEntry._ID, UserContract.UserEntry.COLUMN_USER_ID,
                            UserContract.UserEntry.COLUMN_NAME, UserContract.UserEntry.COLUMN_AGE};
                    Cursor c = db.query(UserContract.UserEntry.TABLE_NAME, projection, UserContract.UserEntry._ID + " = " + "1",
                            null, null, null, null, null);
                    c.moveToFirst();
                    String name = c.getString(c.getColumnIndexOrThrow(UserContract.UserEntry.COLUMN_NAME));
                    c.close();
                    mWebView.loadUrl("javascript:" + callbackFunction + "('" + name + "')");
                }
            });
        }

//        @JavascriptInterface
//        public void showConfirmationDialogNew(String title, String message, final String callbackFunction) {
//            Dialog.OnClickListener positiveListener = new Dialog.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    //mWebView.loadUrl("javascript:" + callbackFunction + "('" + data + "', true)");
//                    mWebView.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            String js = "javascript: var a=" + callbackFunction + ";a(true);";
//                            String url = "javascript:" + Uri.encode(js);
//                            System.out.println(url);
//                            System.out.println("javascript:" + callbackFunction + "(true)");
//                            mWebView.loadUrl(url);
//                            //mWebView.loadUrl("javascript:" + callbackFunction + "(true)");
//                        }
//                    });
//                }
//            };
//
//            Dialog.OnClickListener negativeListener = new Dialog.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    //mWebView.loadUrl("javascript:" + callbackFunction + "('" + data + "', false)");
//                    mWebView.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            String js = "javascript: var a=" + callbackFunction + ";a(false);";
//                            String url = "javascript:" + Uri.encode(js);
//                            mWebView.loadUrl(url);
////                            mWebView.loadUrl("javascript:" + callbackFunction + "(false)");
//                        }
//                    });
//                }
//            };
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            builder
//                    .setTitle(title).setMessage(message)
//                    .setPositiveButton("Ok", positiveListener)
//                    .setNegativeButton("Cancel", negativeListener);
//            builder.create().show();
//        }

        @JavascriptInterface
        public void openWebView(final String url) {
            mWebView.post(new Runnable() {
                @Override
                public void run() {
                    WebViewFragment webViewFragment = WebViewFragment.newInstance(url);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.add(R.id.container, webViewFragment, "webViewFragment");
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
        }

        String address, port;

        @JavascriptInterface
        public void connectSocket(String dstName, String dstPort, final String callbackFunction) {
            //public void connectSocket(final String callbackFunction) {
            if (mSocket != null && !mSocket.isClosed()) {
                try {
                    mSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            SocketCompleteListener listener = new SocketCompleteListener() {
                @Override
                public void onSocketComplete(final boolean response, Socket socket) {
                    mWebView.post(new Runnable() {
                        @Override
                        public void run() {
                            mWebView.loadUrl("javascript:" + callbackFunction + "(" + response + ")");
                        }
                    });
                    mSocket = socket;
                }
            };
            address = dstName;
            port = dstPort;
            SocketTask socketTask = new SocketTask(dstName, Integer.valueOf(dstPort), mSocket, listener);
            socketTask.execute();
        }

        @JavascriptInterface
        public void disconnectSocket(final String callbackFunction) {
            if (mSocket != null && !mSocket.isClosed()) {
                try {
                    mSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    mWebView.post(new Runnable() {
                        @Override
                        public void run() {
                            //mWebView.loadUrl("javascript:" + callbackFunction + "('" + "socket closed" + "')");
                            mWebView.loadUrl("javascript:" + callbackFunction + "(false)");
                        }
                    });
                } finally {
                    mWebView.post(new Runnable() {
                        @Override
                        public void run() {
                            //mWebView.loadUrl("javascript:" + callbackFunction + "('" + "socket closed" + "')");
                            mWebView.loadUrl("javascript:" + callbackFunction + "(true)");
                        }
                    });
                }
            }
        }

        @JavascriptInterface
        public void isSocketConnecting(final String callbackFunction) {
            if (mSocket != null && mSocket.isConnected()) {
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        //mWebView.loadUrl("javascript:" + callbackFunction + "('" + "socket is connecting" + "')");
                        mWebView.loadUrl("javascript:" + callbackFunction + "(true)");
                    }
                });
            } else {
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        //mWebView.loadUrl("javascript:" + callbackFunction + "('" + "socket is disconnected" + "')");
                        mWebView.loadUrl("javascript:" + callbackFunction + "(false)");

                    }
                });

            }
        }

        @JavascriptInterface
        public void reConnect(final String callbackFunction) {
            if (address != null && port != null) {
                if (mSocket != null && mSocket.isConnected()) {
                    mWebView.post(new Runnable() {
                        @Override
                        public void run() {
                            mWebView.loadUrl("javascript:" + callbackFunction + "(true)");
                        }
                    });
                } else {
                    SocketCompleteListener listener = new SocketCompleteListener() {
                        @Override
                        public void onSocketComplete(final boolean response, Socket socket) {
                            mWebView.post(new Runnable() {
                                @Override
                                public void run() {
                                    mWebView.loadUrl("javascript:" + callbackFunction + "(" + response + ")");
                                }
                            });
                            mSocket = socket;
                        }
                    };
                    SocketTask socketTask = new SocketTask(address, Integer.valueOf(port), mSocket, listener);
                    socketTask.execute();
                }
            }
        }
    }

    public WebView getWebView() {
        return mWebView;
    }
}
