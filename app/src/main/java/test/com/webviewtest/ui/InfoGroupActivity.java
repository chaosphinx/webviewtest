package test.com.webviewtest.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import test.com.webviewtest.Constant;
import test.com.webviewtest.R;
import test.com.webviewtest.db.FakeData;
import test.com.webviewtest.model.Group;
import test.com.webviewtest.model.User;
import test.com.webviewtest.ui.widget.ExpandGridAdapter;
import test.com.webviewtest.ui.widget.ExpandGridView;


public class InfoGroupActivity extends AppCompatActivity implements View.OnClickListener {

    private int contactId;
    FakeData mFakeData;
    Group group;

    private TextView tvGroupname;

    // all members number
    private TextView tv_m_total;
    int m_total = 0;

    // members list
    private ExpandGridView gridMembers;

    // switch
    private RelativeLayout reChangeGroupname;
    private RelativeLayout switchTopGroup;
    private RelativeLayout switchBlockGroupmsg;
    private RelativeLayout switchSaveToContact;
    private RelativeLayout switchShowNickName;

    private RelativeLayout reClear;

    // switch state
    private ImageView ivTopGroup;
    private ImageView ivUntopGroup;
    private ImageView ivBlockGroupmsg;
    private ImageView ivUnblockGroupmsg;
    private ImageView ivSaveToContact;
    private ImageView ivUnsaveToContact;
    private ImageView ivShowNickName;
    private ImageView ivHideNickName;

    // quit and delete
    private Button btnExit;

    private String hxid;
    private String group_name;

    boolean is_admin = true;
    List<User> members = new ArrayList<>();

    private String groupId;

    //    private group;
    private ExpandGridAdapter adapter;

    private ProgressDialog progressDialog;
//    private JSONObject jsonObject;
//    private JSONArray jsonarray;

    //    private Map<String, TopUser> topMap = new HashMap<String, TopUser>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_group);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            contactId = bundle.getInt(Constant.KEY_CONTACT_ID);
            Log.i("infocid", String.valueOf(contactId));
        }
        mFakeData = FakeData.getInstance();
//        hxid = LocalUserInfo.getInstance(InfoGroupActivity.this)
//                .getUserInfo("hxid");
//        topMap = DemoApplication.getInstance().getTopUserList();
        initView();
        initFakeData();
        //updateGroupInfo();
    }


    private void initView() {
        //Setup Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(mFakeData.getContactById(contactId).getName());
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        progressDialog = new ProgressDialog(InfoGroupActivity.this);
        tvGroupname = (TextView) findViewById(R.id.tv_groupname);

//        tv_m_total = (TextView) findViewById(R.id.tv_m_total);

        gridMembers = (ExpandGridView) findViewById(R.id.gridview);

        reChangeGroupname = (RelativeLayout) findViewById(R.id.re_change_groupname);
        switchTopGroup = (RelativeLayout) findViewById(R.id.switch_top_group);
        switchBlockGroupmsg = (RelativeLayout) findViewById(R.id.switch_block_groupmsg);
        switchSaveToContact = (RelativeLayout) findViewById(R.id.switch_save_to_contact);
        switchShowNickName = (RelativeLayout) findViewById(R.id.switch_show_nickname);
        reClear = (RelativeLayout) findViewById(R.id.re_clear);

        ivTopGroup = (ImageView) findViewById(R.id.iv_switch_chattotop);
        ivUntopGroup = (ImageView) findViewById(R.id.iv_switch_unchattotop);
        ivBlockGroupmsg = (ImageView) findViewById(R.id.iv_switch_block_groupmsg);
        ivUnblockGroupmsg = (ImageView) findViewById(R.id.iv_switch_unblock_groupmsg);
        ivSaveToContact = (ImageView) findViewById(R.id.iv_switch_save_to_contact);
        ivUnsaveToContact = (ImageView) findViewById(R.id.iv_switch_unsave_to_contact);
        ivShowNickName = (ImageView) findViewById(R.id.iv_show_nickname);
        ivHideNickName = (ImageView) findViewById(R.id.iv_hide_nickname);

        btnExit = (Button) findViewById(R.id.btn_exit_group);
        reChangeGroupname.setOnClickListener(this);
        switchTopGroup.setOnClickListener(this);
        switchBlockGroupmsg.setOnClickListener(this);
        switchSaveToContact.setOnClickListener(this);
        switchShowNickName.setOnClickListener(this);
        reClear.setOnClickListener(this);
        btnExit.setOnClickListener(this);

    }

    private void initFakeData() {
        m_total = 4;
//        tv_m_total.setText("(" + String.valueOf(m_total) + ")");
        group = (Group) mFakeData.getContactById(contactId);
        for (int i = 0; i < group.getUserNumber() - 1; i++) {
            User user = group.getUser(i);
            members.add(user);
        }
        showMembers(members);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void showMembers(List<User> members) {
        adapter = new ExpandGridAdapter(this, members);
        adapter.setIsAdmin(group.isAdmin());
        gridMembers.setAdapter(adapter);

        // quit delete mode when clicking gridView
        gridMembers.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (adapter.isDeleteMode) {
                            adapter.isDeleteMode = false;
                            adapter.notifyDataSetChanged();
                            return true;
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.switch_top_group:
                if (ivTopGroup.getVisibility() == View.VISIBLE) {
                    ivTopGroup.setVisibility(View.INVISIBLE);
                    ivUntopGroup.setVisibility(View.VISIBLE);

//                    if (topMap.containsKey(group.getGroupId())) {
//                        topMap.remove(group.getGroupId());
//                        TopUserDao topUserDao = new TopUserDao(
//                                ChatRoomSettingActivity.this);
//
//                        topUserDao.deleteTopUser(group.getGroupId());
//                    }
                } else {
                    ivTopGroup.setVisibility(View.VISIBLE);
                    ivUntopGroup.setVisibility(View.INVISIBLE);

//                    if (!topMap.containsKey(group.getGroupId())) {
//                        TopUser topUser = new TopUser();
//                        topUser.setTime(System.currentTimeMillis());
//                        // 1---stands for group
//                        topUser.setType(1);
//                        topUser.setUserName(group.getGroupId());
//                        Map<String, TopUser> map = new HashMap<String, TopUser>();
//                        map.put(group.getGroupId(), topUser);
//                        topMap.putAll(map);
//                        TopUserDao topUserDao = new TopUserDao(
//                                ChatRoomSettingActivity.this);
//                        topUserDao.saveTopUser(topUser);\
//                    }
                }
                break;
            case R.id.switch_block_groupmsg:
                // if already block group
                if (ivBlockGroupmsg.getVisibility() == View.VISIBLE) {
                    ivBlockGroupmsg.setVisibility(View.INVISIBLE);
                    ivUnblockGroupmsg.setVisibility(View.VISIBLE);
                } else {
                    ivBlockGroupmsg.setVisibility(View.VISIBLE);
                    ivUnblockGroupmsg.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.switch_save_to_contact:
                if (ivSaveToContact.getVisibility() == View.VISIBLE) {
                    ivSaveToContact.setVisibility(View.INVISIBLE);
                    ivUnsaveToContact.setVisibility(View.VISIBLE);
                } else {
                    ivSaveToContact.setVisibility(View.VISIBLE);
                    ivUnsaveToContact.setVisibility(View.INVISIBLE);
                }
                break;

            case R.id.switch_show_nickname:
                if (ivShowNickName.getVisibility() == View.VISIBLE) {
                    ivShowNickName.setVisibility(View.INVISIBLE);
                    ivHideNickName.setVisibility(View.VISIBLE);
                } else {
                    ivShowNickName.setVisibility(View.VISIBLE);
                    ivHideNickName.setVisibility(View.INVISIBLE);
                }
                break;

            case R.id.re_clear: // clear group messages
                progressDialog.setMessage("leaning...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                clearGroupHistory();
                break;
            case R.id.re_change_groupname:
//                showNameAlert();
                break;

            case R.id.btn_exit_group:
                //deleteMembersFromGroup(hxid);
                break;

            default:
                break;
        }
    }

    public void clearGroupHistory() {
    }

    protected void updateGroupInfo() {
        new Thread(new Runnable() {
            public void run() {
                try {
//                    Group returnGroup = GroupManager.getInstance()
//                            .getGroupFromServer(groupId);
//                    // update local data
//                    GroupManager.getInstance().createOrUpdateLocalGroup(
//                            returnGroup);

                    runOnUiThread(new Runnable() {
                        public void run() {
//                            if (group != null) {
//                                System.out.println("group msg is blocked:"
//                                        + group.getMsgBlocked());
//                                // initial group state
//                                if (group.getMsgBlocked()) {
//                                    ivBlockGroupmsg.setVisibility(View.VISIBLE);
//                                    ivUnblockGroupmsg.setVisibility(View.INVISIBLE);
//                                } else {
//                                    ivBlockGroupmsg.setVisibility(View.INVISIBLE);
//                                    ivUnblockGroupmsg.setVisibility(View.VISIBLE);
//                                }

//                                if (topMap.containsKey(group.getGroupId())) {
//                                    ivTopGroup.setVisibility(View.VISIBLE);
//                                    ivUntopGroup.setVisibility(View.INVISIBLE);
//                                } else {
//                                    ivTopGroup.setVisibility(View.INVISIBLE);
//                                    ivUntopGroup.setVisibility(View.VISIBLE);
//                                }
//                            }
                        }
                    });

                } catch (Exception e) {
                    runOnUiThread(new Runnable() {
                        public void run() {

                        }
                    });
                }
            }
        }).start();
    }

    protected void deleteMembersFromGroup(final String username) {
        final ProgressDialog deleteDialog = new ProgressDialog(
                InfoGroupActivity.this);
        // quit group
        if (hxid.equals(username)) {
//            deleteDialog.setMessage("Quitting...");
//            deleteDialog.setCanceledOnTouchOutside(false);
//            deleteDialog.show();
//            // non-group owner
//            if (!is_admin) {
//                try {
//                    JSONObject newJSON = new JSONObject();
//                    newJSON.put("groupname", group_name);
//                    for (int n = 0; n < jsonarray.size(); n++) {
//                        JSONObject jsontemp = (JSONObject) jsonarray.get(n);
//                        if (jsontemp.getString("hxid").equals(username)) {
//                            jsonarray.remove(jsontemp);
//                        }
//                    }
//
//                    newJSON.put("jsonArray", jsonarray);
//                    String updateStr = newJSON.toJSONString();

//                    updateGroupName(groupId, updateStr);
//                    GroupManager.getInstance().exitFromGroup(groupId);
//                    deleteDialog.dismiss();
//                    Toast.makeText(ChatRoomSettingActivity.this, "success",
//                            Toast.LENGTH_LONG).show();
//                    setResult(100);
//                    finish();
//                } catch (EaseMobException e) {
//                    deleteDialog.dismiss();
//                    Toast.makeText(ChatRoomSettingActivity.this, "fail",
//                            Toast.LENGTH_LONG).show();
//                    e.printStackTrace();
//                }
//            }
//            // group owner
//            else {
//                try {
//                    GroupManager.getInstance().exitAndDeleteGroup(groupId);
//                    deleteDialog.dismiss();
//                    Toast.makeText(ChatRoomSettingActivity.this, "success",
//                            Toast.LENGTH_LONG).show();
//                    setResult(100);
//                    finish();
//                } catch (EaseMobException e) {
//                    deleteDialog.dismiss();
//                    Toast.makeText(ChatRoomSettingActivity.this, "fail",
//                            Toast.LENGTH_LONG).show();
//                    e.printStackTrace();
//                }
//            }
        }
        // removing member
        else {
            deleteDialog.setMessage("Removing...");
            deleteDialog.setCanceledOnTouchOutside(false);
            deleteDialog.show();
//            try {
//                GroupManager.getInstance().removeUserFromGroup(groupId,
//                        username);
            for (int i = 0; i < members.size(); i++) {
                User user = members.get(i);
                if (user.getUserName().equals(username)) {
                    members.remove(user);
                    adapter.notifyDataSetChanged();
                    m_total = members.size();
                    tv_m_total.setText("(" + String.valueOf(m_total) + ")");
//                        JSONObject newJSON = new JSONObject();
//                        newJSON.put("groupname", group_name);
//                        for (int n = 0; n < jsonarray.size(); n++) {
//                            JSONObject jsontemp = (JSONObject) jsonarray.get(n);
//                            if (jsontemp.getString("hxid").equals(username)) {
//                                jsonarray.remove(jsontemp);
//                            }
//                        }
//
//                        newJSON.put("jsonArray", jsonarray);
//                        String updateStr = newJSON.toJSONString();
//                        Log.e("updateStr------>>>>>0", updateStr);
//
//                        GroupManager.getInstance().changeGroupName(groupId,
//                                updateStr);
                }
            }

            deleteDialog.dismiss();
            Toast.makeText(InfoGroupActivity.this, "success",
                    Toast.LENGTH_LONG).show();
//            }
//            catch (EaseMobException e) {
//                deleteDialog.dismiss();
//                Toast.makeText(InfoGroupActivity.this, "fail",
//                        Toast.LENGTH_LONG).show();
//                e.printStackTrace();
//            }
        }
    }

    private void updateGroupName(String groupId, String updateStr) {
//        Map<String, String> map = new HashMap<String, String>();
//        map.put("groupId", groupId);
//        map.put("groupName", updateStr);
//        LoadDataFromServer task = new LoadDataFromServer(
//                ChatRoomSettingActivity.this, Constant.URL_UPDATE_Groupnanme,
//                map);

//        task.getData(new DataCallBack() {
//            @Override
//            public void onDataCallBack(JSONObject data) {
//                if (data != null) {
//                    int code = data.getInteger("code");
//
//                    if (code != 1) {

//                    }
//                }
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
