package test.com.webviewtest.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import test.com.webviewtest.Constant;
import test.com.webviewtest.R;
import test.com.webviewtest.Utils.Utils;
import test.com.webviewtest.db.FakeData;
import test.com.webviewtest.model.Contact;
import test.com.webviewtest.model.Conversation;
import test.com.webviewtest.ui.adapter.AdapterDivider;
import test.com.webviewtest.ui.adapter.ContactAdapter;

public class ContactFragment extends Fragment implements ContactAdapter.onItemClickListener {
    EditText mEtSearchBar;
    private ContactAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    RecyclerView mRecyclerView;
    List<Contact> contactList;
    FakeData mFakeData;

    public ContactFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contact, null);
        mEtSearchBar = (EditText) v.findViewById(R.id.et_search_bar);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.contact_list);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ContactAdapter(getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new AdapterDivider(getActivity(), R.drawable.list_divider));
        return v;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFakeData = FakeData.getInstance();
        contactList = new ArrayList<>();
        contactList = mFakeData.getContactList();
        Collections.sort(contactList, new Utils.PinyinComparator() {
        });
    }


    @Override
    public void onItemClick(int i) {
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        Contact contact = contactList.get(i);
        int contactId = contact.getContactId();
        intent.putExtra(Constant.KEY_CONTACT_ID, contactId);
        for (Conversation c : mFakeData.getConversationList()) {
            if (c.getSender().getContactId() == contactId) {
                intent.putExtra("conversation", mFakeData.getConversationList().indexOf(c));
            } else { //no conversation with this contact
                intent.putExtra("conversation", -1);
            }
        }
        if (contact.isGroup()) {
            intent.putExtra(Constant.KEY_CHAT_TYPE, ChatActivity.CHAT_TYPE_GROUP);
        } else {
            intent.putExtra(Constant.KEY_CHAT_TYPE, ChatActivity.CHAT_TYPE_PERSON);
//            User user = (User) contact;
//            intent.putExtra("userId", user.getUserId());
//            intent.putExtra("sign", user.getSign());
//            intent.putExtra("avatar", user.getAvatar());
        }
        startActivity(intent);
    }
}