package test.com.webviewtest.db;

import android.provider.BaseColumns;

public final class UserContract {
    public static final String SQL_CREATE_ENTRIES =
            "create table " + UserEntry.TABLE_NAME + " (" +
                    UserEntry._ID + " integer primary key," +
                    UserEntry.COLUMN_USER_ID + " text," +
                    UserEntry.COLUMN_NAME + " text," +
                    UserEntry.COLUMN_AGE + " text" +
                    " );";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + UserEntry.TABLE_NAME;

    public UserContract() {
    }

    public static abstract class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "user_table";
        public static final String COLUMN_USER_ID = "user_id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_AGE = "age";
    }
}
