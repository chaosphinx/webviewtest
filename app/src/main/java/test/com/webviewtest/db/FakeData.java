package test.com.webviewtest.db;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import test.com.webviewtest.Utils.Utils;
import test.com.webviewtest.model.Contact;
import test.com.webviewtest.model.Conversation;
import test.com.webviewtest.model.Group;
import test.com.webviewtest.model.Message;
import test.com.webviewtest.model.User;


public class FakeData {

    public static FakeData instance = null;
    List<Contact> contactList;
    List<Conversation> conversationList;
    static int contactId = 0;

    private FakeData() {
        contactList = new ArrayList<>();
        conversationList = new ArrayList<>();
        initFakeContact();
        initFakeConversation();
    }

    public static FakeData getInstance() {
        if (instance == null) {
            synchronized (FakeData.class) {
                if (instance == null) {
                    instance = new FakeData();
                }
            }
        }
        return instance;
    }

    public void initFakeContact() {
        // fake contact data
        List<Contact> contactList = new ArrayList<>();
        String[] ss = new String[]{"李一", "李二", "李三", "李四", "张一", "张二", "张三", "LiYi", "LiEr", "Friedman",
                "Mises", "Stephen", "Helvetica", "Roboto", "Crismon", "Vollkorn", "Slabo", "Test", "Some really looooong name", "119"};
        // person contact
        for (int i = 0; i < 15; i++) {
            User user = new User();
            user.setContactId(contactId++);
            user.setUserId(String.valueOf(i));
            user.setUserName(ss[i]);
            user.setName(ss[i]);
            user.setAvatar("avatar" + i);
            user.setSign("signature" + i);
            user.setTel("132465789" + i);
            Utils.initUserHeader(user);
            contactList.add(user);
        }
        // group contact
        for (int i = 0; i < 3; i++) {
            Group group = new Group();
            group.setContactId(contactId++);
            group.setName("群组" + i);
            for (int j = 0; j < Utils.randInt(2, 12); j++) {
                group.addUser((User) contactList.get(j));
            }
            contactList.add(group);
        }

        //a small group
        Group groupSmall = new Group();
        groupSmall.setContactId(contactId++);
        groupSmall.setName("人数较少的群组");
        for (int j = 0; j < 2; j++) {
            groupSmall.addUser((User) contactList.get(j));
        }
        contactList.add(groupSmall);

        //a large group
        Group groupLarge = new Group();
        groupLarge.setContactId(contactId++);
        groupLarge.setName("人数较多的群组-群主");
        for (int j = 0; j < 13; j++) {
            groupLarge.addUser((User) contactList.get(j));
        }
        groupLarge.setIsAdmin(true);
        contactList.add(groupLarge);
        setContactList(contactList);

    }

    private void initFakeConversation() {
        List<Conversation> conversationList = new ArrayList<>();
        // unread personal conversation
        for (int i = 0; i < 2; i++) {
            Conversation converUnreadPerson = new Conversation(contactList.get(i));
            converUnreadPerson.setUnreadMsgCount(Utils.randInt(1, 20));
            for (int msgCount = 0; msgCount < converUnreadPerson.getUnreadMsgCount(); msgCount++) {
                converUnreadPerson.addMessage(new Message(converUnreadPerson.getSender().getName(), "2015-08-05 18:00",
                        "未读个人对话" + msgCount, true));
            }
            conversationList.add(converUnreadPerson);
        }

        // unread group conversation
        Conversation converUnreadGroup = new Conversation(contactList.get(15));
        converUnreadGroup.setGroup(true);
        converUnreadGroup.setUnreadMsgCount(100);
        for (int msgCount = 0; msgCount < converUnreadGroup.getUnreadMsgCount(); msgCount++) {
            Group group = (Group) converUnreadGroup.getSender();
            converUnreadGroup.addMessage(new Message(group.getUser(Utils.randInt(0, group.getUserNumber() - 2)).getName(),
                    "2015-08-05 18:00",
                    "未读群组对话" + msgCount, true));
        }
        conversationList.add(converUnreadGroup);

        // normal personal conversation
        Conversation converPerson = new Conversation(contactList.get(3));
        for (int msgCount = 0; msgCount < 15; msgCount++) {
            if (msgCount % 2 == 0) {
                converPerson.addMessage(new Message(converPerson.getSender().getName(), "2015-08-05 18:00",
                        "已读个人对话" + msgCount, true));
            } else {
                converPerson.addMessage(new Message(" ", "2015-08-05 18:00",
                        "已读群组对话" + msgCount, false));
            }
        }
        conversationList.add(converPerson);

        // normal group conversation
        Conversation converGroup = new Conversation(contactList.get(19));
        converGroup.setGroup(true);
        for (int msgCount = 0; msgCount < 20; msgCount++) {
            Group group = (Group) converGroup.getSender();
            if (msgCount % 3 == 0) {
                converGroup.addMessage(new Message(" ", "2015-08-05 18:00", "对话内容" + msgCount, false));
            }
            converGroup.addMessage(new Message(group.getUser(Utils.randInt(0, group.getUserNumber() - 2)).getName(),
                    "2015-08-05 18:00",
                    "对话内容" + msgCount, true));
        }
        conversationList.add(converGroup);

        setConversationList(conversationList);
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List list) {
        contactList = list;
    }


    public Contact getContactById(int id) {
        for (Contact c : contactList) {
            if (c.getContactId() == id) {
                return c;
            }
        }
        return null;
    }

    public void setConversationList(List list) {
        conversationList = list;
    }

    public List<Conversation> getConversationList() {
        return conversationList;
    }

    public void addGroup(Group group) {
        group.setContactId(contactId++);
        group.setName("新建群" + String.valueOf(contactId - 1));
        contactList.add(group);
    }

    public void addConversation(Conversation c) {
        conversationList.add(0, c);
    }
}
