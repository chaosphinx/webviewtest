package test.com.webviewtest.Task;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import test.com.webviewtest.Interface.SocketCompleteListener;

public class SocketTask extends AsyncTask<Void, Void, Void> {
    Socket socket;
//    String response;
    boolean response;
    String mAddress;
    int mPort;
    SocketCompleteListener mListener;

    public SocketTask(String dstName, int dstPort, Socket socket, SocketCompleteListener listener) {
        super();
        mAddress = dstName;
        mPort = dstPort;
        this.socket = socket;
        mListener = listener;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            socket = new Socket(mAddress, mPort);
            InputStream inputStream = socket.getInputStream();
            int bytesRead;
            byte[] buffer = new byte[1024];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
//                response += byteArrayOutputStream.toString("UTF-8");
                response=true;
            }


            // another way to receive data
//            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//            String msg = in.readLine();
//            if (msg != null) {
//                response = msg;
//                System.out.println(msg);
//            } else {
//                response = "data error";
//            }
//            in.close();

        } catch (UnknownHostException e) {
            Log.e("UnknownHostException", e.toString());
            response=false;
//            response = "UnknownHostException: " + e.toString();
        } catch (IOException e) {
            Log.e("IOException", e.toString());
            response=false;
//            response = "IOException: " + e.toString();
        }
        // TODO: remove close
//        finally {
//            if (socket != null) {
//                try {
//                    socket.close();
//                } catch (IOException e) {
//                    Log.e("close", "IOException: " + e.toString());
//                }
//            }
//        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mListener.onSocketComplete(response, socket);
    }
}
