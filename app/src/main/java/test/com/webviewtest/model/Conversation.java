package test.com.webviewtest.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Conversation {
    List<Message> messageList;
    private int unreadMsgCount = 0;
    private String username;
    private boolean isGroup = false;
    private Contact sender = null;

    public Conversation(Contact sender) {
        this.sender = sender;
        messageList = new ArrayList<>();
    }

    public Conversation(Contact sender, boolean isGroup) {
        this.sender = sender;
        this.isGroup = isGroup;
        if (this.messageList == null) {
            this.messageList = Collections.synchronizedList(new ArrayList());
        }
    }

    Conversation(String sender, List<Message> messageList, boolean isGroup) {
        this.username = sender;
        this.isGroup = isGroup;
        if (this.messageList == null) {
            this.messageList = Collections.synchronizedList(messageList);
        }
    }

    public void addMessage(Message message) {
//            if(message.getChatType() == ChatType.GroupChat) {
//                this.isGroup = true;
//            }
        this.messageList.add(message);
    }

    void addMessage(Message var1, boolean var2) {
//            if(var1.getChatType() == ChatType.GroupChat) {
//                this.isGroup = true;
//            }

        if (this.messageList.size() > 0) {
            Message var3 = (Message) this.messageList.get(this.messageList.size() - 1);
            if (var1.getMsgId() != null && var3.getMsgId() != null && var1.getMsgId().equals(var3.getMsgId())) {
                return;
            }
        }

        boolean var6 = false;
        Iterator var5 = this.messageList.iterator();

        while (var5.hasNext()) {
            Message var4 = (Message) var5.next();
            if (var4.getMsgId().equals(var1.getMsgId())) {
                var6 = true;
                break;
            }
        }

        if (!var6) {
            this.messageList.add(var1);
        }

    }

    public Contact getSender() {
        return sender;
    }


    void saveUnreadMsgCount(final int var1) {
//            EMChatManager.getInstance().msgCountThreadPool.submit(new Runnable() {
//                public void run() {
//                    c.a().a(EMConversation.this.username, var1);
//                }
//            });
    }

    void deleteUnreadMsgCountRecord() {
    }

    public int getUnreadMsgCount() {
        if (this.unreadMsgCount < 0) {
            this.unreadMsgCount = 0;
        }
        return this.unreadMsgCount;
    }

    public void setUnreadMsgCount(int unreadMsgCount) {
        this.unreadMsgCount = unreadMsgCount;
    }

    public void resetUnsetMsgCount() {
        this.unreadMsgCount = 0;
        this.saveUnreadMsgCount(0);
    }

    public void resetUnreadMsgCount() {
        this.unreadMsgCount = 0;
        this.saveUnreadMsgCount(0);
    }

    public int getMsgCount() {
        return messageList.size();
    }

    public Message getMessage(int position) {
        if (position >= messageList.size()) {
            Log.e("conversation", "outofbound, messageList.size:" + this.messageList.size());
            return null;
        } else {
            return messageList.get(position);
        }
    }


    public Message getMessage(String text) {
        for (int count = messageList.size() - 1; count >= 0; --count) {
            Message msg = messageList.get(count);
            if (msg.getMsgId().equals(text)) {
                return msg;
            }
        }
        return null;
    }

    public List<Message> getAllMessages() {
        return messageList;
    }

    public String getUserName() {
        return this.username;
    }

    public boolean getIsGroup() {
        return this.isGroup;
    }

    public boolean isGroup() {
        return this.isGroup;
    }

    public void setGroup(boolean isGroup) {
        this.isGroup = isGroup;
    }

    public Message getLastMessage() {
        return messageList.size() == 0 ? null : messageList.get(messageList.size() - 1);
    }

    public void clear() {
        this.messageList.clear();
        this.unreadMsgCount = 0;
        //c.a().j(this.username);
    }
}
