package test.com.webviewtest.model;


import test.com.webviewtest.Utils.Utils;

public class Contact {
    private int contactId;
    private String userId;
    private String name;
    private String avatar;
    public boolean isGroup;
    private String header;

    public Contact() {
    }

    public Contact(int id, String name) {
        contactId = id;
        this.name = name;
        Utils.initUserHeader(this);
    }


    public void setContactId(int id) {
        this.contactId = id;
    }

    public int getContactId() {
        return contactId;
    }

    public void setUserId(String id) {
        this.userId = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setName(String name) {
        this.name = name;
        Utils.initUserHeader(this);
    }

    public String getName() {
        return name;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setIsGroup(boolean isGroup) {
        this.isGroup = isGroup;
    }

    public boolean getIsGroup() {
        return isGroup;
    }

    public boolean isGroup() {
        return isGroup;
    }

}
