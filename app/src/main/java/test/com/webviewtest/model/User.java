package test.com.webviewtest.model;

public class User extends Contact {
//    private String userId;
    private String userName;
    private int unreadMsgCount;
    private String header;
    private String usernick;
    private String sex;
    private String tel;
    private String fxid;
    private String region;
    private String avatar;
    private String sign;
    private String beizhu;

    public User() {
        isGroup = false;
    }

//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    public String getUserId() {
//        return userId;
//    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

//    public String getHeader() {
//        return header;
//    }
//
//    public void setHeader(String header) {
//        this.header = header;
//    }

    public int getUnreadMsgCount() {
        return unreadMsgCount;
    }

    public void setUnreadMsgCount(int unreadMsgCount) {
        this.unreadMsgCount = unreadMsgCount;
    }

    public void setName(String usernick) {
        this.usernick = usernick;
    }

    public String getName() {
        return usernick;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTel() {
        return tel;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void setFxid(String fxid) {
        this.fxid = fxid;
    }

    public String getFxid() {
        return fxid;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
    }

    public String getBeizhu() {
        return beizhu;
    }

    @Override
    public int hashCode() {
        return 17 * getUserName().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof User)) {
            return false;
        }
        return getUserName().equals(((User) o).getUserName());
    }

    @Override
    public String toString() {
        return userName;
    }

    @Override
    public boolean getIsGroup() {
        return false;
    }

    @Override
    public boolean isGroup() {
        return false;
    }
}
