package test.com.webviewtest.model;

public class Message {
    private static final String TAG = Message.class.getSimpleName();

    private String msgId;


    private int senderContectId;

    private User senderUser;

    // obsoleted
    private String name;

    private String date;

    private String text;

    private boolean isComMeg = true;

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getMsgId() {
        return msgId;
    }

    public int getSenderContactId() {
        return senderContectId;
    }

    public void setSenderContactId(int senderContectId) {
        this.senderContectId = senderContectId;
    }

    public User getSenderUser() {
        return senderUser;
    }

    public void setSenderUser(User senderUser) {
        this.senderUser = senderUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getMsgType() {
        return isComMeg;
    }

    public void setMsgType(boolean isComMsg) {
        isComMeg = isComMsg;
    }

    public Message() {
    }

    public Message(String name, String date, String text, boolean isComMsg) {
        super();
        this.name = name;
        this.date = date;
        this.text = text;
        this.isComMeg = isComMsg;
    }

    public Message(int senderContectId, String date, String text, boolean isComMsg) {
        super();
        this.senderContectId = senderContectId;
        this.date = date;
        this.text = text;
        this.isComMeg = isComMsg;
    }

    public Message(User user, String date, String text, boolean isComMsg) {
        super();
        this.senderUser = user;
        this.date = date;
        this.text = text;
        this.isComMeg = isComMsg;
    }

}