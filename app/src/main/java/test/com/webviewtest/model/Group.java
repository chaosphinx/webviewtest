package test.com.webviewtest.model;

import java.util.ArrayList;
import java.util.List;

public class Group extends Contact {
    List<User> userList;
    boolean isAdmin = false;

    public Group() {
        userList = new ArrayList<>();
        isGroup = true;
    }

    public User getUser(int position) {
        return userList.get(position);
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> list) {
        userList = list;
    }

    public void addUser(User user) {
        userList.add(user);
    }

    public void removeUser(User user) {
        if (userList.contains(user)) {
            userList.remove(user);
        }
    }

    public int getUserNumber() {
        return userList.size() + 1;
    }

    @Override
    public boolean getIsGroup() {
        return true;
    }

    @Override
    public boolean isGroup() {
        return true;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
}
