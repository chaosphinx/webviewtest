package test.com.webviewtest;

public class Constant {
    public static final String KEY_CHAT_TYPE = "conversation_type";
    public static final String NEW_FRIENDS_USERNAME = "item_new_friends";
    public static final String GROUP_USERNAME = "item_groups";
    public static final String MESSAGE_ATTR_IS_VOICE_CALL = "is_voice_call";
    public static final String ACCOUNT_REMOVED = "account_removed";

    public static final String KEY_CONTACT_ID="contact_id";
}
