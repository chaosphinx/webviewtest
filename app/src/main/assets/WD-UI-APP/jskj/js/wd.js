function notifyToast(webURL, msg) {
	var xhr = new plus.net.XMLHttpRequest();
	xhr.onreadystatechange = xhrStatechange;
	xhr.setRequestHeader("Content-Type", "text/html;charset=utf-8");
	xhr.open("GET", webURL);
	xhr.send();

	function xhrStatechange() {
		switch (xhr.readyState) {
			case 4:
				if (xhr.status == 200) {
					var resp = JSON.parse(xhr.responseText);
					if (resp == null) {
						console.error("检查JSON，字符串报错");
					} else {
						console.log(JSON.stringify(resp));
						if (resp.result == "true") {
							plus.nativeUI.toast(msg);
						}
					}
				}
		}
	}
}