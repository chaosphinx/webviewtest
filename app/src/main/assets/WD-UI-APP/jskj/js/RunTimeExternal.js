document.addEventListener("plusready", function() {
	var _BARCODE = 'RunTimeExternal',
		B = window.plus.bridge;
	var RunTimeExternal = {
		OpenFile: function(Argus1, Argus2, Argus3, successCallback, errorCallback) {
			var success = typeof successCallback !== 'function' ? null : function(args) {
					successCallback(args);
				},
				fail = typeof errorCallback !== 'function' ? null : function(code) {
					errorCallback(code);
				};
			callbackID = B.callbackId(success, fail);

			return B.exec(_BARCODE, "OpenFile", [callbackID, Argus1, Argus2, Argus3]);
		}
	};
	window.plus.RunTimeExternal = RunTimeExternal;
}, true);

function openDocoment(fileName) {
	var dtask = plus.downloader.createDownload(fileName, {}, function(d, status) {
		if (status == 200) {
			plus.RunTimeExternal.OpenFile(plus.io.convertLocalFileSystemURL(d.filename), "cn.wps.moffice_eng", "cn.wps.moffice.documentmanager.PreStartActivity", function(e) {}, function(e) {
				plus.nativeUI.confirm("检查到您未安装\"WPS Office\"，是否到商城搜索下载？", function(i) {
					if (i.index == 0) {
						plus.runtime.openURL("market://details?id=cn.wps.moffice_eng");
					}
				});
			});
		} else {
			alert("检查网络是否连接");
		}
	});
	dtask.start();
}