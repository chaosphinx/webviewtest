document.addEventListener( "plusready",  function()
{
                          
      var unRequest ={
        ReciveVerify:function(argv1){
            alert("111");
        }
      };
    
    window.plus.unRequest =unRequest;
    
    var _BARCODE = 'jstxapi',
		B = window.plus.bridge;
    var jstx =
    {
    	Login : function (uname,  successCallback, errorCallback )
		{
			var success = typeof successCallback !== 'function' ? null : function(args) 
			{
				successCallback(args);
			},
			fail = typeof errorCallback !== 'function' ? null : function(code) 
			{
				errorCallback(code);
			};
			callbackID = B.callbackId(success, fail);

			return B.exec(_BARCODE, "net_Login", [callbackID, uname]);
		},
          OfflineLogin : function (uname,  successCallback, errorCallback )
          {
              var success = typeof successCallback !== 'function' ? null : function(args)
              {
              successCallback(args);
              },
              fail = typeof errorCallback !== 'function' ? null : function(code)
              {
              errorCallback(code);
              };
              callbackID = B.callbackId(success, fail);
              
              return B.exec(_BARCODE, "net_offlineLogin", [callbackID, uname]);
          },
        SearchUser : function (keywords,  successCallback, errorCallback )
        {
            var success = typeof successCallback !== 'function' ? null : function(args)
            {
            successCallback(args);
            },
            fail = typeof errorCallback !== 'function' ? null : function(code)
            {
            errorCallback(code);
            };
            callbackID = B.callbackId(success, fail);

            return B.exec(_BARCODE, "net_SearchUser", [callbackID, keywords]);
        },
      AddFriend : function (uid,msg,  successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
          
          return B.exec(_BARCODE, "net_AddFriend", [callbackID, uid ,msg]);
      },
      AcceptFriend : function (uid, successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
          
          return B.exec(_BARCODE, "net_AcceptFriend", [callbackID, uid ]);
      },
      AddGroupToContact : function (gid, successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
      
          return B.exec(_BARCODE, "net_AddGroupToContact", [callbackID, gid ]);
      },
      AddUserToContact : function (uid, successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
          
          return B.exec(_BARCODE, "net_AddUserToContact", [callbackID, uid ]);
      },
      DeleteUserFromContact : function (uid, successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
          
          return B.exec(_BARCODE, "net_DeleteUserFromContact", [callbackID, uid ]);
      },
      DeleteGroupFromContact : function (gid, successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
          
          return B.exec(_BARCODE, "net_DeleteGroupFromContact", [callbackID, gid ]);
      },
      CreateGroup : function (lsuid, successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
          
          return B.exec(_BARCODE, "net_CreateGroup", [callbackID, lsuid ]);
      },
      LoadVerify : function ( successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
          return B.exec(_BARCODE, "net_LoadVerify", [callbackID ]);
      },
      OpenSession : function (sid,pagelimit,  successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
          
          return B.exec(_BARCODE, "net_OpenSession", [callbackID, sid ,pagelimit]);
      },
      GetMessage : function (sid,lastid,pagelimit,  successCallback, errorCallback )
      {
          var success = typeof successCallback !== 'function' ? null : function(args)
          {
          successCallback(args);
          },
          fail = typeof errorCallback !== 'function' ? null : function(code)
          {
          errorCallback(code);
          };
          callbackID = B.callbackId(success, fail);
          
          return B.exec(_BARCODE, "net_getMessageFromSession", [callbackID, sid ,lastid,pagelimit]);
      },

        TEST : function ( successCallback, errorCallback )
        {
            var success = typeof successCallback !== 'function' ? null : function(args)
            {
            successCallback(args);
            },
            fail = typeof errorCallback !== 'function' ? null : function(code)
            {
            errorCallback(code);
            };
            callbackID = B.callbackId(success, fail);
            return B.exec(_BARCODE, "testfunc", [callbackID]);
        }
    };
    window.plus.jstx = jstx;
                          
      var jstx_mem =
      {
          GetSelf : function ( )
          {
             return B.execSync(_BARCODE, "GetSelf", []);
          },
          GetContactUser : function ( )
          {
          return B.execSync(_BARCODE, "GetContactUser", []);
          },
          GetUserInfo : function (uid )
          {
          return B.execSync(_BARCODE, "GetUser", [uid]);
          },
          GetContactGroup : function ( )
          {
          return B.execSync(_BARCODE, "GetContactGroup", []);
          },
          GetGroupInfo:function(gid)
          {
          return B.execSync(_BARCODE, "GetGroup", [gid]);
          },
          GetVerifyList:function()
          {
            return B.execSync(_BARCODE, "GetVerifyList", []);
          },
          GetSessionList:function()
          {
             return B.execSync(_BARCODE,"GetSessionList",[]);
          },
          SendTextMessage:function(sid,text)
          {
          return B.execSync(_BARCODE,"SendTextMessage",[sid,text]);
          },
          AttentionContact:function()
          {
          return B.execSync(_BARCODE, "AttentionContact", []);
          },
          AttentionSessions:function(){
            return B.execSync(_BARCODE, "AttentionSessionUpdate", []);
          },
          AttentionConnect:function()
          {
          return B.execSync(_BARCODE, "AttentionConnect", []);
          },
          CloseSession:function(sid){
           return B.execSync(_BARCODE, "CloseSession", [sid]);
          }
      };
    window.plus.jstx_mem = jstx_mem;
             
                          
                          
}, true );