
	function openActivity(activity){
		Android.openActivity(activity);
	}

	function loadUrl(url){
		Android.loadUrl(url);
	}

	function openWebView(url){
		Android.openWebView(url);
	}

    function showDialog (title, content, callbackFunction) {
		Android.showConfirmationDialog(title, content, callbackFunction);
	}

//	function showDialogNew(title, content) {
//		Android.showConfirmationDialogNew(function(result){
//			if (result) {
//				document.getElementById("confirmation").innerHTML = "ok";
//			} else {
//				document.getElementById("confirmation").innerHTML = "cancel";
//			}
//    	}, title, content);
//	}

	function showDatePicker (dateCallback){
		Android.showDatePicker(dateCallback);
	}


	function getPreference(key){
		return Android.getPreference(key);
	}

	function getDatabase(callbackFunction){
		Android.getDatabase(callbackFunction);
	}

	function openCamera(){
		Android.openCamera();
	}

	function connectSocket(address, port, callbackFunction){
		Android.connectSocket(address, port, callbackFunction);
	}


	function disconnectSocket(callbackFunction){
		Android.disconnectSocket(callbackFunction);
	}

	function isSocketConnecting(callbackFunction){
		Android.isSocketConnecting(callbackFunction);
	}

	function reconnect(callbackFunction){
		Android.reConnect(callbackFunction);
	}
