// 设置制定面板全屏显示（高度为设备高度）
function fullScreen(objId) {
	setTimeout(function() {
		var sliderparent = document.getElementById(objId);
		var pxstr = document.documentElement.clientHeight - pageY(sliderparent);
		sliderparent.style.height = pxstr + "px";
	}, 200);
}

// 获取页面元素坐标位置-X坐标
function pageX(elem) {
	return elem.offsetParent ? (elem.offsetLeft + pageX(elem.offsetParent)) : elem.offsetLeft;
}

// 获取页面元素坐标位置-Y坐标
function pageY(elem) {
	return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
}

(function($) {
	// 处理弹出框关闭按钮样式
	if (document.getElementById("ad-gb") != null && document.getElementById("ad-gb") != undefined) {
		document.getElementById("ad-gb").classList.remove("icon-cuowu");
		document.getElementById("ad-gb").classList.add("icon-iconfont43");
	}
})(mui);